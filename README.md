# incremental backups into s3

Every Bucket represents a backup session and is named by date.

Consequent backups of a directory will be backed up to a new Bucket but all
objects in all Buckets will be compared to minimise uploading the same file
twice.

## Dependencies:
 - boto3
   - Uses `boto3.client('s3')` so a correct credentials file must be in place.
 - click

## Usage
`s3bu.py BACKUP_ORIGIN`

### Example
`s3bu /home/user`

