"""s3bu.py - incremental backups into s3
 - an s3 version of hlbu - hard link backups
 author: chadwick rolfs
 date: 08 nov 2020

an origin directory will have each file uploaded into s3
subsequent backups will skip what has been uploaded
 - but will have a new bucket with the current date

args: backup_origin - directory to back up

dir_backup as s3
 - s3 buckets named by date
   - s3 objects named by file path on origin
   - s3 object have tagging
     - adler 32 hash
     - lstat
       - size
       - st_mtime
       - st_ctime

the origin directory is walked
  - every file becomes a Backupfile object
all s3 buckets will be queried for their objects
 - each object becomes a BackupFile object
origin BackupFiles not in s3 BackupFiles
 - will be uploaded to a new s3 bucket
"""
import os
import click

import inventory
from s3 import S3

click_origin_type = click.Path(exists=True, file_okay=False)

@click.command()
@click.argument('backup_origin', nargs=1, type=click_origin_type)
def s3bu(backup_origin):
    '''s3bu, backup incrementally to s3, skipping existing backups'''

    origin_files = inventory.get_origin_files(backup_origin)

    s3_0 = S3()
    backup_files = s3_0.get_all_objects()

    s3_0.backup_new_files(set(origin_files) - set(backup_files))

if __name__ == '__main__':
    s3bu()
