import boto3
from backup_item import BackupFile

def get_backup_today():
    datetime_today = datetime.datetime.today()
    return datetime_today.strftime('%Y-%m-%d-%H-%M')

class S3:
    def __init__(self, backup_today=None):
        self.s3client = self.get_s3_client()
        self.backup_today = backup_today
        if backup_today is None:
            self.backup_today = get_backup_today()
        self.backup_bucket = None

    def get_s3_client(self):
        return boto3.client('s3')

    def get_all_objects(self):
        '''return a list of all objects in all buckets'''
        s3buckets = convert_buckets(self.s3client.list_buckets())
        all_objects = []
        for bucket in s3buckets:
            # TODO: pagination if more than 1000 objects
            # https://gitlab.com/mrwick/s3bu/-/issues/4
            s3objects = self.s3client.list_objects_v2(Bucket=bucket)
            for s3object in s3objects:
                tagging = convert_tagging(
                        s3object.get_object_tagging())
                all_objects.append(
                        BackupFile(s3object.key, tagging))

        return all_objects

    def convert_tagging(self, tagging):
        '''flatten tagging to {key: value} from the TagSet in the aws response'''
        tags = tagging['TagSet']
        tag_dict = {}
        for tag in tags:
            tag_dict[tag['Key']] = tag['Value']
        return tag_dict

    def convert_buckets(self, buckets):
        '''flatten buckets to [Name, Name, ..] from the dictionary in the aws response'''
        return [b['Name'] for b in bd['Buckets']].sort()

    def get_latest_backup(self):
        '''return last bucket in bucket list'''
        return convert_buckets(
                self.s3client.list_buckets())[-1]

    def new_backup(self):
        '''create a new s3 bucket with today's date converted to year-month-day-hour-minute'''
        try:
            self.s3client.create_bucket(Bucket=self.backup_today)
        except BucketAlreadyExists:
            date = f'{date}_0'
            self.s3client.create_bucket(Bucket=date)

    def get_backup_bucket(self):
        '''return backup bucket for date in backup_today'''
        return s3client.Bucket(self.backup_today)

    def backup_new_file(self, new_file):
        '''upload file to latest backup bucket'''
        if self.backup_bucket is None:
            self.new_backup()
            self.backup_bucket = get_backup_bucket()

        # todo multipart if file > than 100MB
        self.backup_bucket.upload_file(
                Filename=new_file.file_path,
                Key=file_path)
        self.s3client.put_object_tagging(
                Bucket=self.backup_bucket.name,
                Key=new_file.file_path,
                TagSet=[
                    {'file_a32': new_file.file_a32},
                    {'file_stat': {
                        'st_size': new_file.st_size,
                        'st_mtime': new_file.st_mtime,
                        'st_ctime': new_file.st_ctime,
                        }
                    }
                ]
        )

    def backup_new_files(self, new_files):
        for new_file in new_files:
            backup_new_file(new_file)
