import os
import zlib

class Backup:
    def __init__(self, file_path):
        self.file_path = file_path

class BackupFile:
    def __init__(self, file_path, tagging=None):
        self.backup = Backup(file_path)
        self.file_path = self.backup.file_path
        if tagging is not None:
            self.file_stat = s3lstat(tagging['file_stat'])
            self.file_a32 = tagging['file_a32']
        else:
            self.file_stat = os.lstat(file_path)
            self.file_a32 = self._hash_file(file_path)

    @property
    def file_size(self):
        return self.file_stat.st_size

    @property
    def file_mtime(self):
        return self.file_stat.st_mtime

    @property
    def file_ctime(self):
        return self.file_stat.st_ctime

    def _hash_file(self, file_path):
        with open(self.file_path, 'rb') as file_handle:
            return zlib.adler32(file_handle.read())

    def __repr__(self):
        return f'{self.file_a32} {self.file_path}'

    def __eq__(self, other):
        return self.file_a32 == other.file_a32

    def __hash__(self):
        return self.file_a32

class s3lstat:
    def __init__(self, tagging):
        self.tagging = tagging

    @property
    def st_size(self):
        return self.tagging['st_size']

    @property
    def st_mtime(self):
        return self.tagging['st_mtime']

    @property
    def st_ctime(self):
        return self.tagging['st_ctime']
