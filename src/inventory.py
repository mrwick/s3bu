"""inventory.py

get_origin_files - walk directory to backup
"""

import os
from backup_item import BackupFile

def get_origin_files(dir_origin):
    '''create a list for the files walked in the backup origin path for each file, append to the list a BackupFile object

    dir_origin: this path will be passed to os.walk'''
    origin_files = list()
    origin_walk = os.walk(dir_origin)

    for (orig_path, orig_dirs, orig_files) in origin_walk:
        for orig_file in orig_files:
            orig_file_path = os.path.join(orig_path, orig_file)
            origin_files.append(BackupFile(orig_file_path))

    return origin_files
