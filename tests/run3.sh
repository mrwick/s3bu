#!/bin/bash
set -e

# pilfered from openmotics

export PYTHONPATH=$PYTHONPATH:$(pwd)/../src
pytest .
