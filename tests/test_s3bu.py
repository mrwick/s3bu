'''s3bu tests
'''
import os
import unittest
import zlib
import backup_item

class TestBackupItem(unittest.TestCase):
    def setUp(self):
        self.test_file = 'test_file'
        self.test_file2 = 'test_file2'

        with open(self.test_file, 'w') as tfo:
            tfo.write('test')

        with open(self.test_file, 'rb') as test_file_handle:
            self.a32_test = zlib.adler32(test_file_handle.read())

        with open(self.test_file2, 'w') as tfo:
            tfo.write('test')

        self.backup_file1 = backup_item.BackupFile(self.test_file)
        self.backup_file2 = backup_item.BackupFile(self.test_file2)

    def tearDown(self):
        del self.backup_file1
        del self.backup_file2
        del self.a32_test
        os.remove(self.test_file)
        os.remove(self.test_file2)


    def test_backup_file1_file_path(self):
        self.assertEqual(self.backup_file1.file_path, self.test_file)

    def test_backup_file1_is_BackupFile(self):
        self.assertIsInstance(
                self.backup_file1, backup_item.BackupFile)

    def test_backup_file1_adler32(self):
        self.assertEqual(
                self.backup_file1.file_a32, self.a32_test)

    def test_backup_file1_lstat(self):
        test_lstat = os.lstat(self.test_file)
        self.assertEqual(self.backup_file1.file_stat, test_lstat)

    def test_eq__(self):
        assert self.backup_file2 == self.backup_file1

    def test_is_not(self):
        assert self.backup_file2 is not self.backup_file1

    def test__hash_file(self):
        assert self.a32_test == self.backup_file1._hash_file(
                self.test_file)

if __name__ == '__main__':
    unittest.main()
